package com.pajato.edgar.memoizer.installer

import com.pajato.edgar.memoizer.core.BaseTest
import com.pajato.edgar.memoizer.core.Config
import java.io.File
import kotlin.test.Test
import kotlin.test.assertEquals

class InstallTest : BaseTest() {

    @Test fun `When the app is not installed, verify the predicate`() {
        File(Config.configBaseDir, AppJarFileName).delete()
        assertEquals(false, appIsInstalled())
    }

    @Test fun `When the app is installed, verify the predicate`() {
        File(Config.configBaseDir, AppJarFileName).delete()
        installApp()
        assertEquals(true, appIsInstalled())
    }
}
