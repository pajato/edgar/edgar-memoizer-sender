package com.pajato.edgar.memoizer.sender

import com.pajato.edgar.logger.Log
import com.pajato.edgar.memoizer.core.BaseTest
import com.pajato.edgar.memoizer.core.Config
import com.pajato.edgar.memoizer.core.MemoizerCommand
import com.pajato.edgar.memoizer.core.getMemoizerPid
import com.pajato.edgar.memoizer.core.memoizerIsRunning
import com.pajato.edgar.memoizer.installer.AppJarFileName
import com.pajato.edgar.memoizer.installer.installApp
import java.io.File
import kotlin.test.Test
import kotlin.test.assertFalse
import kotlin.test.assertTrue

class SenderTest : BaseTest() {

    @Test fun `When sending an exit command with the app not installed, verify the app gets installed`() {
        val command = MemoizerCommand("test()", "exit")
        val channel = Config.commandsSendFile.channel
        assertFalse(memoizerIsRunning())
        File(Config.configBaseDir, AppJarFileName).delete()
        sendCommand(command, channel)
        assertTrue(memoizerIsRunning())
    }

    @Test fun `When sending an exit command with the receiver app installed, verify the app gets started`() {
        val command = MemoizerCommand("test()", "exit")

        fun getMessageThatShowsFailureFromApp(): String {
            val memoizerPid: Long = getMemoizerPid()
            val process: Process = Config.launcher.process
            val processPid = process.pid()
            val result = StringBuilder()

            fun append(message: String) = result.append(message).append("\n")

            fun getNotRunningMessage(pid: Long) = "Memoizer (pid: $pid) is not running!".also { append(it) }

            fun getAliveMessage(pid: Long) = "Process (pid: $pid) is alive: ${process.isAlive}.".also { append(it) }

            fun getExitValueMessage() = "Failed with exit code: ${process.exitValue()}.".also { append(it) }

            Log.add(getNotRunningMessage(memoizerPid))
            Log.add(getAliveMessage(processPid))
            if (!process.isAlive) Log.add(getExitValueMessage())
            return result.toString()
        }

        assertFalse(memoizerIsRunning())
        File(Config.configBaseDir, AppJarFileName).delete()
        installApp()
        sendCommand(command)
        assertFalse(memoizerIsRunning(), getMessageThatShowsFailureFromApp())
    }
}
