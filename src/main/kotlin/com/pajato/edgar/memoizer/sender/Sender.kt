package com.pajato.edgar.memoizer.sender

import com.pajato.edgar.memoizer.core.Config
import com.pajato.edgar.memoizer.core.Config.commandsSendFile
import com.pajato.edgar.memoizer.core.MemoizerCommand
import com.pajato.edgar.memoizer.core.launcher.launch
import com.pajato.edgar.memoizer.core.memoizerIsRunning
import com.pajato.edgar.memoizer.core.sender.sendCommandToRunningApp
import com.pajato.edgar.memoizer.installer.appIsInstalled
import com.pajato.edgar.memoizer.installer.installApp
import java.nio.channels.AsynchronousCloseException
import java.nio.channels.ClosedByInterruptException
import java.nio.channels.FileChannel

/**
 * Send a command to be executed by the Memoizer process.
 *
 * @param command The command to be executed by the memoizer.
 * @param channel The FileChannel object dealing with the IPC (optional, for testing).
 *
 * @return a diagnostic message if the function fails for any reason.
 *
 * @throws ClosedByInterruptException when the channel has been closed by interrupt
 * @throws AsynchronousCloseException when the channel ...
 */
fun sendCommand(command: MemoizerCommand, channel: FileChannel = commandsSendFile.channel) {
    if (!appIsInstalled()) installApp()
    if (!memoizerIsRunning()) launch(Config.configBaseDir.absolutePath)
    sendCommandToRunningApp(command, channel)
}
