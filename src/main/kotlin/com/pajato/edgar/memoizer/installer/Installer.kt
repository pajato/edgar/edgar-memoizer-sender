package com.pajato.edgar.memoizer.installer

import com.pajato.edgar.memoizer.core.Config
import java.io.File
import java.io.InputStream
import java.io.OutputStream

internal const val AppJarFileName = "app.jar"

internal fun appIsInstalled(): Boolean = File(Config.configBaseDir, AppJarFileName).exists()

internal fun installApp() {
    fun copy(inputStream: InputStream, outputStream: OutputStream) {
        inputStream.copyTo(outputStream)
        inputStream.close()
        outputStream.close()
    }

    fun getAppJarAsInputStream(): InputStream {
        val loader: ClassLoader = object {}.javaClass.enclosingClass.classLoader
        return loader.getResourceAsStream("app.jar")
    }

    fun getConfigPathAsOutputStream(): OutputStream = File(Config.configBaseDir, AppJarFileName).outputStream()

    copy(getAppJarAsInputStream(), getConfigPathAsOutputStream())
}
